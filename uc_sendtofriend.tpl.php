<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#657C91" style="font-family: verdana, arial, helvetica; font-size: small;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">
        <tr valign="top">
          <td bgcolor="#657C91" style="color: white;">
            <table width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
              <tr>
                <td>
                  <img src="<?php print $website_logo; ?>" alt="<?php print $store_name; ?>"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr valign="top">
          <td>
            <p>
              Hello!
            </p>
            <p>
              A friend of yours sent you a free copy of <a href="<?php print $product_link; ?>"><?php print $product_name; ?></a> from <?php print $store_name; ?>!
            </p>
            <?php if (!empty($message)): ?>
            <br/>
            <table width="85%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#BECBDE" style="font-family: verdana, arial, helvetica">
              <tr>
                <td bgcolor="#BECBDE">
                  <p>
                    <b>A personal message was sent along with the product:</b>
                  </p>
                  <p>
                    <em><?php print $message; ?></em>
                  </p>
                </td>
              </tr>
            </table>
            <br/>
            <?php endif; ?>
            <p>
              <b>Downloading Your Gift:</b><br/>
              <ol>
                <li>Login to your account with <?php print $store_name; ?> here: <a href="<?php print $website_login_link_link; ?>">login</a> (see below for login credentials)</li>
                <li>After logging in, click the "Files" tab or the link that says "Click here to view your file downloads"</li>
                <li>Download the files from the product you received and enjoy!</li>
              </ol>
            </p>
            <p>
              <?php if ($new): ?>
              Your username is: <b><?php print $friend_username; ?></b><br/>
              Your password is: <b><?php print $friend_password; ?></b>
              <?php else: ?>
              Please be aware that you already have an account on the website with username <b><?php print $friend_username; ?></b>.<br />
              If you forgot your password, you can <a href="<?php print $password_reset_link; ?>">reset it here</a>.
              <?php endif; ?>
              </p>
            </p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
