
This module provides a checkout pane for sending products (with downloads) to a friend during checkout.

It is useful for holiday promotions to have your customers send products to their friends and family.

Features:
- Configure checkout pane wording and limit what products are available to send
- Customize an HTML email template that gets send to the friend
- Allow customers to provide personal message and specific date to send the email
- Order pane provided to administrators with info regarding the send-to-a-friend details for an order
- Order & Admin comments left indicating when emails were processed

NOTE:
Right now, this module will send ALL files associated with a product ID and has no support for different
files being included in different SKUs.